using System;
using Xunit;
using test_unit_first_time.Controllers;
using System.Linq;

namespace XUnitTestProject_first_time
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var con100 = new ValuesController();
            var result = con100.Get();
        
            Assert.Equal(2, result.Count());
        }
        
    }
}
